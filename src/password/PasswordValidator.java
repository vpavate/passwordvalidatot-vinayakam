package password;

import static org.junit.Assert.assertFalse;

/*
 * @Author Vinayak Pavate
 * Student ID: 991548217
 * This class validates passwords and it will be developed using TDD
 * @param password
 * @return
 */
public class PasswordValidator {
	
	private static int MIN_NUM_DIGITS =2 ;
		public static boolean hasEnoughDigits(String password) {		
			int count = 0;
			for (int i = 0, len = password.length(); i < len; i++) {
			    if (Character.isDigit(password.charAt(i))) {
			        count++;
			    }
			}
			if(password != null) {
				if(count >=MIN_NUM_DIGITS ) {
					return true;
				}
			}
			return false;
		
	}
		private static int MIN_LENGTH =8 ;
		public static boolean isValidLength(String password) {	
			if(password != null) {
				return password.trim().length() >= MIN_LENGTH;
			}
			return false;
		}
		}




